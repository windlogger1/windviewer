# Description de l'idée de base

## Vue web
backend : python flask sqlite
frontend : bootstrap + [chart](https://mdbootstrap.com/javascript/charts/)
template : bootstrap simple base template responsive
data: anemo + girouette + puissance ac + puissance dc

 1. 4 blocs textes
 2. 4 graphs/widgets
 3. vue plus personnalisées pour chaques données (autres data + details)

## Modèle
1. json data pair key:value
2. ??

### Controleur
API :
  1. remplissage de la base de données par get
  2. retour embeded

numerotation
vue+modele+controleur  = 000

En cours :

 1. *mettre en place la base de données* > done
 2. *Mettre en place le système de login* > done
 2. *mettre en place l'API pour remplir la base de données* >done
 3. afficher les données dans les boites
 4. Mettre à jour périodiquement

#### objectif api

 |method | URI | Explication |
|----|-------------------|--------------|
|GET |	/api/users/<id> |	Return a user.|
|GET |	/api/users |	Return the collection of all users.|
|POST |	/api/users |	Register a new user account.|
|PUT |	/api/users/<id> |	Modify a user. |
|GET |	/api/users/<id>/feeds/<fe> |	Return a feed fe owned by user id.|
|GET |	/api/users/<id>/feeds |	Return the collection of all feeds of user id.|
|POST |	/api/users/<id>/feeds/<fe> |	Feed a feed fe of user id account.|
|POST |	/api/users/<id>/feeds |	Feed feeds of user id account.|


Inputs
idPrimary 	int(11)
	2 	userid 	int(11)
	3 	name 	text 	utf8_general_ci
	4 	description 	text 	utf8_general_ci
	5 	nodeid 	text 	utf8_general_ci
	6 	processList 	text 	utf8_general_ci
	7 	time 	int(10)
	8 	value 	float


Feeds
1 	idPrimary 	int(11)
	2 	name 	text 	utf8_general_ci
	3 	userid 	int(11)
	4 	tag 	text 	utf8_general_ci
	5 	time 	int(10)
	6 	value 	double
	7 	datatype 	int(11)
	8 	public 	tinyint(1)
	9 	size 	int(11)
	10 	engine 	int(11)
	11 	processList 	text

  ## API
 * create a ne user :

    `http POST http://localhost:5000/api/users username=gilou password=gigi email=gigi@example.com`

    ```console
    HTTP/1.0 201 CREATED
    Content-Length: 90
    Content-Type: application/json
    Date: Sun, 12 Aug 2018 14:55:33 GMT
    Location: http://localhost:5000/api/users/1
    Server: Werkzeug/0.14.1 Python/3.6.6

    {
        "_links": {
            "feeds": "/api/users/1/feeds",
            "self": "/api/users/1"
        },
        "id": 1,
        "username": "gilou"
    }
    ```

 * Connect with user/password and receive token :

    `http --auth gilou:gigi POST http://localhost:5000/api/tokens`

    ```console
    HTTP/1.0 200 OK
    Content-Length: 45
    Content-Type: application/json
    Date: Sun, 12 Aug 2018 14:58:12 GMT
    Server: Werkzeug/0.14.1 Python/3.6.6

    {
        "token": "Et2Qn7H5wsdQbE5JfI2GVMaglVXaA3zL"
    }
    ```

 * Request users list :

    `http GET http://localhost:5000/api/users "Authorization:Bearer Et2Qn7H5wsdQbE5JfI2GVMaglVXaA3zL"`

    ```console
    HTTP/1.0 200 OK
    Content-Length: 241
    Content-Type: application/json
    Date: Sun, 12 Aug 2018 15:06:18 GMT
    Server: Werkzeug/0.14.1 Python/3.6.6

    {
        "_links": {
            "next": null,
            "prev": null,
            "self": "/api/users?page=1&per_page=10"
        },
        "_meta": {
            "page": 1,
            "per_page": 10,
            "total_items": 1,
            "total_pages": 1
        },
        "items": [
            {
                "_links": {
                    "feeds": "/api/users/1/feeds",
                    "self": "/api/users/1"
                },
                "id": 1,
                "username": "gilou"
            }
        ]
    }
    ```

 * Request user(id) :

    `http GET http://localhost:5000/api/users/1 "Authorization:Bearer Et2Qn7H5wsdQbE5JfI2GVMaglVXaA3zL"`

    ```console
    HTTP/1.0 200 OK
    Content-Length: 90
    Content-Type: application/json
    Date: Sun, 12 Aug 2018 14:59:39 GMT
    Server: Werkzeug/0.14.1 Python/3.6.6

    {
        "_links": {
            "feeds": "/api/users/1/feeds",
            "self": "/api/users/1"
        },
        "id": 1,
        "username": "gilou"
    }
    ```
 * Feed a feed :

  
