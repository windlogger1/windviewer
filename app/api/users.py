from app.api import bp
from app import db
from flask import jsonify, request, url_for
from app.models import User, Feed
from app.api.errors import bad_request
from app.api.auth import token_auth

@bp.route('/users/<int:id>', methods=['GET'])
@token_auth.login_required
def get_user(id):
    return jsonify(User.query.get_or_404(id).to_dict())

@bp.route('/users', methods=['GET'])
@token_auth.login_required
def get_users():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = User.to_collection_dict(User.query, page, per_page, 'api.get_users')
    return jsonify(data)

@bp.route('/users', methods=['POST'])
def create_user():
    data = request.get_json() or {}
    if 'username' not in data or 'email' not in data or 'password' not in data:
        return bad_request('must include username, email and password fields')
    if User.query.filter_by(username=data['username']).first():
        return bad_request('please use a different username')
    if User.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    user = User()
    user.from_dict(data, new_user=True)
    db.session.add(user)
    db.session.commit()
    response = jsonify(user.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_user', id=user.id)
    return response

@bp.route('/users/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_user(id):
    user = User.query.get_or_404(id)
    data = request.get_json() or {}
    if 'username' in data and data['username'] != user.username and \
            User.query.filter_by(username=data['username']).first():
        return bad_request('please use a different username')
    if 'email' in data and data['email'] != user.email and \
            User.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    user.from_dict(data, new_user=False)
    db.session.commit()
    return jsonify(user.to_dict())


@bp.route('/users/<int:id>/feeds/<string:name>', methods=['GET'])
@token_auth.login_required
def get_feed(id,name):
    user = User.query.get_or_404(id)
    for feed in user.feeds:
        if feed.name==name:
            return jsonify(feed.to_dict())
    return bad_request('feed not found')

@bp.route('/users/<int:id>/feeds', methods=['GET'])
@token_auth.login_required
def get_feeds(id):
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = User.to_collection_dict(user.feeds, page, per_page, 'api.get_feeds', id=id)
    return jsonify(data)

# Post feed content, create if necessary
@bp.route('/users/<int:id>/feeds', methods=['POST'])
@token_auth.login_required
def feed_feed(id):
    user = User.query.get_or_404(id)
    data = request.get_json() or {}
    if data == None:
        return bad_request('request must include value at least')
    for feed in user.feeds :
        if feed.name==data['name']:
            if 'value' not in data:
                return bad_request('must include value field to update it')
            feed.from_dict(data)
            db.session.commit()
            return jsonify(feed.to_dict())
    if 'name' not in data and 'type' not in data and 'value' not in data and 'unit' not in data:
        return bad_request('must include feed name, type, value and unit fields to create it')
    new_feed = Feed()
    new_feed.from_dict(data)
    db.session.add(new_feed)
    db.session.commit()
    response = jsonify(new_feed.to_dict())
    response.status_code = 201
    response.headers['location'] = url_for('api.get_feed', id=user.id, name = new_feed.name)




@bp.route('/users/<int:id>/feeds', methods=['POST'])
@token_auth.login_required
def feed_feeds():
    pass
